const express = require('express');
const bodyParser = require('body-parser');

const App = express();

const routesPeru = require('./routes/routesPeru');



App.use(bodyParser.json());
App.use(bodyParser.urlencoded({extended: false}));

App.use('/datosCliente',routesPeru);


module.exports = App;