module.exports = {
  PORT: process.env.PORT || 3000,
  DB: process.env.DB || 'mongodb://localhost:32770/datosPeru',
}