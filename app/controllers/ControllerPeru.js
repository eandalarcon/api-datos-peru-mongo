const datosCliente = require('../models/modelPeru');

function index(req,res){
    datosCliente.find({})
        .then(datosClientes => {
            if(datosClientes.length) return res.status(200).send({datosClientes});
            return res.status(204).send({message: 'NO CONTENT'});
        }).catch(error => res.status(500).send({error}));
}

function show(req,res){
    if(req.body.error) return res.status(500).send({error});
    if(!req.body.datosClientes) return res.status(404).send({message: 'NOT FOUND'});
    let datosClientes = req.body.datosClientes;
    return res.status(200).send({datosClientes});
    
}

function create(req,res){
    new datosCliente(req.body).save().then(datosCliente => res.status(201).send({datosCliente})).catch(error => res.status(500).send({error}));
}

function update(req,res){
    if(req.body.error) return res.status(500).send({error});
    if(!req.body.datosClientes) return res.status(404).send({message: 'NOT FOUND'});
    let datosCliente = req.body.datosClientes[0];
    datosCliente = Object.assign(datosCliente,req.body);
    datosCliente.save().then(datosCliente => res.status(200).send({message: "UPDATED", datosCliente})).catch(error => res.status(500).send({error}));
}

function remove(req,res){
    if(req.body.error) return res.status(500).send({error});
    if(!req.body.datosClientes) return res.status(404).send({message: 'NOT FOUND'});
    req.body.datosClientes[0].remove().then(datosCliente => res.status(200).send({message: 'REMOVED', datosCliente})).catch(error => res.status(500).send({error}));
}

function find(req,res,next){
    let query = {};
    query[req.params.key] = req.params.value;
    datosCliente.find(query).then(datosClientes => {
        if(!datosClientes.length) return next();
        req.body.datosClientes = datosClientes;
        return next();
    }).catch(error =>{
        req.body.error = error;
        next();
    })
}

module.exports = {
    index,
    show,
    create,
    update,
    remove,
    find
}