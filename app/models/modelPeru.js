const mongoose = require('mongoose');

const modelPerutSchema = new mongoose.Schema({
    identificationType: {
        type: Number,
        required: true
    },
    identificationNumber: {
        type: Number,
        unique: true,
        required: true
    },
    ipClient: {
        type: String,
        required: true
    },
    registerDate: {
        type: Date,
        default: Date.now(),
        required: true
    },
    registerHour: {
        type: Date,
        default: Date.now(),
        required: true
    },
    checkLegal: {
        type: Boolean,
        required: true,
        default: true
    },
    Channel:{
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: Number,
        required: true
    }
    
    
});

const modelPeru = mongoose.model('datosCliente',modelPerutSchema);

module.exports = modelPeru;