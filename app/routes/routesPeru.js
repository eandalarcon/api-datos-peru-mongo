const express = require('express');
const ControllerPeru = require('../controllers/ControllerPeru');

const Router = express.Router();

Router.get('/',ControllerPeru.index)
      .post('/',ControllerPeru.create)
      .get('/:key/:value',ControllerPeru.find,ControllerPeru.show)
      .put('/:key/:value',ControllerPeru.find,ControllerPeru.update)
      .delete('/:key/:value',ControllerPeru.find,ControllerPeru.remove);

module.exports = Router;