const dataBase = require('./app/config/database');
const config = require('./app/config/config');
const App = require('./app/app');

dataBase.connect();

App.listen(config.PORT, function(error){
  if(error) return console.log(error);
  console.log(`Servidor corriendo en el Puerto: ${config.PORT}`)
})